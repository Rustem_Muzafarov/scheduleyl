package com.dev.klvra.scheduleyl;

import android.app.SearchManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.klvra.scheduleyl.database.DatabaseAccess;
import com.dev.klvra.scheduleyl.fragments.Fragment_Cities;
import com.dev.klvra.scheduleyl.fragments.Fragment_Districts;
import com.dev.klvra.scheduleyl.fragments.Fragment_Favorite;
import com.dev.klvra.scheduleyl.fragments.Fragment_Gardens;
import com.dev.klvra.scheduleyl.fragments.Fragment_Search;
import com.dev.klvra.scheduleyl.fragments.Fragment_Suburb;
import com.dev.klvra.scheduleyl.fragments.Fragment_about_Route;

import java.util.ArrayList;


public class SchedActivity extends AppCompatActivity {
    Intent intent;
    Bundle bundle;
    ActionBarDrawerToggle drawerToggle;
    Fragment_Gardens fragment_gardens;
    Fragment_Cities fragment_cities;
    Fragment_Districts fragment_districts;
    Fragment_Favorite fragment_favorite;
    Fragment_Suburb fragment_suburb;
    Fragment_about_Route fragment_about_route;
    Fragment_Search fragment_search;
    DrawerLayout drawerLayout;
    SearchView searchView;
    DatabaseAccess databaseAccess;
    Toolbar toolbar;
    NavigationView navigationView;
     public int favoritecounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sched);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        Menu menu = navigationView.getMenu();

        setSupportActionBar(toolbar);
        databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        ArrayList<Route_Common> list = databaseAccess.getFavoriteList();
        favoritecounter = list.size();

        if (favoritecounter!=0){
            fragment_favorite = new Fragment_Favorite();
            getSupportFragmentManager().beginTransaction().add(R.id.routesFragment,fragment_favorite).commit();
            setFavoriteCounter(favoritecounter);
            navigationView.getMenu().getItem(0).setChecked(true);
        }
        else {
            fragment_cities = new Fragment_Cities();
            getSupportFragmentManager().beginTransaction().add(R.id.routesFragment,fragment_cities).commit();
            navigationView.getMenu().getItem(2).setChecked(true);
        }
        databaseAccess.close();
        drawerToggle = setupDrawerToggle();
        setupDrawerContent(navigationView);
        setCounters(16, 33, 45, 18);
    }


    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        searchView = (SearchView) MenuItemCompat.
                getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Введите маршрут...");
        searchView.setOnQueryTextListener(querytextlistener);
        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                Log.d("MyTag","Search Expanded");
                fragment_search = new Fragment_Search();
                replaceFragment(fragment_search);
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                Log.d("MyTag","Search Hide");
                onBackPressed();
            }
        });
        return true;
    }

    SearchView.OnQueryTextListener querytextlistener =new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            if (newText.length()>0){
                final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
                databaseAccess.open();
                ArrayList<Route_Common> list = databaseAccess.searchInDB(newText);
                databaseAccess.close();
                fragment_search.listChanged(list);
            }
            return false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch(item.getItemId()){
            case android.R.id.home:
                if(!searchView.isIconified()) {
                    onBackPressed();
                }
                else onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate( Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener(){
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem){
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void routeClick(String route, int period){
                    bundle = new Bundle();
            bundle.putString("route", route);
            bundle.putInt("period", period);
            fragment_about_route = new Fragment_about_Route();
            fragment_about_route.setArguments(bundle);
            replaceFragment(fragment_about_route);
            drawerToggle.setDrawerIndicatorEnabled(false);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(route);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void favoriteStateChange(String route, boolean favoritestatus){
        final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        if(favoritestatus){
            databaseAccess.changeFavoriteStatusToTrue(route);
            favoritecounter++;
        }
        else{
            databaseAccess.changeFavoriteStatusToFalse(route);
            favoritecounter--;
        }
        setFavoriteCounter(favoritecounter);
        databaseAccess.close();
    }

    public void replaceFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.routesFragment, fragment).addToBackStack(null).commit();
    }

    public void selectDrawerItem(MenuItem menuItem){
        switch(menuItem.getItemId()){
            case R.id.item_favorite:
                fragment_favorite = new Fragment_Favorite();
                replaceFragment(fragment_favorite);
                break;
            case R.id.item_districts:
                fragment_districts = new Fragment_Districts();
                replaceFragment(fragment_districts);
                break;
            case R.id.item_cities:
                fragment_cities = new Fragment_Cities();
                replaceFragment(fragment_cities);
                break;
            case R.id.item_suburb:
                fragment_suburb = new Fragment_Suburb();
                replaceFragment(fragment_suburb);
                break;
            case R.id.item_gardens:
                fragment_gardens = new Fragment_Gardens();
                replaceFragment(fragment_gardens);
                break;
            case R.id.item_about_routes:
                Toast.makeText(SchedActivity.this, "О маршрутах", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_help:
                String number = "8362450305";
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                startActivity(intent);
                break;
            case R.id.item_inaccuracy:
                Toast.makeText(SchedActivity.this, "Ошибочка", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_about_app:
                Toast.makeText(SchedActivity.this, "О приложении", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item_letter:
                String email = "klvra.05@gmail.com";
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, email);
                startActivity(intent);
                break;
        }
        if(menuItem.getGroupId()==R.id.menu_top) {
            menuItem.setChecked(true);
        }
        setTitle(menuItem.getTitle());
        drawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            onBackPressed();
        }
        super.onBackPressed();
        drawerToggle.setDrawerIndicatorEnabled(true);
    }

    private void setMenuCounter(@IdRes int itemId, int count) {         ///NavigationViewCounters
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
        view.setText(count > 0 ? String.valueOf(count) : null);
    }

    private void setCounters(int districts, int cities, int suburbs, int gardens){
        setMenuCounter(R.id.item_districts, districts);
        setMenuCounter(R.id.item_cities, cities);
        setMenuCounter(R.id.item_suburb, suburbs);
        setMenuCounter(R.id.item_gardens, gardens);
    }

    private void setFavoriteCounter(int count){
        setMenuCounter(R.id.item_favorite, count);
    }

    public void expandToolbar(){
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, false);
    }


}



