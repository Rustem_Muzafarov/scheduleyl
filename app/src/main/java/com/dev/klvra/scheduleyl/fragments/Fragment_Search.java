package com.dev.klvra.scheduleyl.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.klvra.scheduleyl.adapters.CustomRecyclerViewAdapter;
import com.dev.klvra.scheduleyl.R;
import com.dev.klvra.scheduleyl.RecViewDecoration;
import com.dev.klvra.scheduleyl.Route_Common;

import java.util.ArrayList;

public class Fragment_Search extends Fragment {
    ArrayList<Route_Common> list;
    RecyclerView recyclerView;
    Bundle bundle;
    CustomRecyclerViewAdapter adapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MyTag","Search  OnCreate");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Поиск");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recycleview, null);
        recyclerView =(RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new RecViewDecoration(getActivity()));
        return v;

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new CustomRecyclerViewAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
    }

    public void listChanged(ArrayList<Route_Common> list){
        recyclerView.setAdapter(new CustomRecyclerViewAdapter(getActivity(),list));
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d("MyTag","Search  OnPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("MyTag", "Search  OnResume");
    }
}
