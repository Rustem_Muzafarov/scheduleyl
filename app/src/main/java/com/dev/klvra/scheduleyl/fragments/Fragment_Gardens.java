package com.dev.klvra.scheduleyl.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.klvra.scheduleyl.adapters.CustomRecyclerViewAdapter;
import com.dev.klvra.scheduleyl.R;
import com.dev.klvra.scheduleyl.RecViewDecoration;
import com.dev.klvra.scheduleyl.Route_Common;
import com.dev.klvra.scheduleyl.database.DatabaseAccess;

import java.util.ArrayList;


public class Fragment_Gardens extends Fragment {

    ArrayList<Route_Common> list;
    RecyclerView recyclerView;
    DatabaseAccess databaseAccess;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseAccess = DatabaseAccess.getInstance(getActivity());
        databaseAccess.open();
        list = databaseAccess.getRoutesList("gardens");
        databaseAccess.close();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("В сады");
        Log.d("MyTag","Gardens OnCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recycleview, null);
        recyclerView =(RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new RecViewDecoration(getActivity()));
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CustomRecyclerViewAdapter adapter = new CustomRecyclerViewAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("В сады");
        databaseAccess.open();
        list = databaseAccess.getRoutesList("gardens");
        databaseAccess.close();
        CustomRecyclerViewAdapter adapter = new CustomRecyclerViewAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
        Log.d("MyTag","Gardens OnResume");
    }

}