package com.dev.klvra.scheduleyl.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.klvra.scheduleyl.R;
import com.dev.klvra.scheduleyl.Route_Info_More;

import java.util.ArrayList;


public class NonPeriodicRecAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private Context context;
    private ArrayList<Route_Info_More> list = new ArrayList<>();
    private final int TIMEITEM = 0;
    private final int LASTITEM = 2;
    private final int EMPTYITEM = 1;

    public NonPeriodicRecAdapter (Context context, ArrayList<Route_Info_More> list){
        this.list = list;
        this.context = context;
    }

    public class PeriodHolder extends RecyclerView.ViewHolder{      //standart item class
        protected TextView hour;
        protected TextView minute1;
        protected TextView minute2;
        protected TextView minute3;
        protected TextView minute4;
        protected TextView note;

        public PeriodHolder (View view){
            super(view);
            this.hour = (TextView) view.findViewById(R.id.hour);
            this.minute1 = (TextView) view.findViewById(R.id.minute1);
            this.minute2 = (TextView) view.findViewById(R.id.minute2);
            this.minute3 = (TextView) view.findViewById(R.id.minute3);
            this.minute4 = (TextView) view.findViewById(R.id.minute4);
            this.note = (TextView) view.findViewById(R.id.note);
        }
    }

    public class LastItemHolder extends RecyclerView.ViewHolder{        //last item class
        protected TextView firstrow;
        protected TextView secondrow;
        protected TextView thirdrow;
        protected TextView numberrow;

        public LastItemHolder(View view){
            super(view);
            this.firstrow = (TextView) view.findViewById(R.id.firstrow);
            this.secondrow = (TextView) view.findViewById(R.id.secondrow);
            this.thirdrow = (TextView) view.findViewById(R.id.thirdrow);
            this.numberrow = (TextView) view.findViewById(R.id.numberrow);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(context);

        switch (viewType) {
            case TIMEITEM:
                View v1 = inflater.inflate(R.layout.info_about_list_item, viewGroup, false);
                viewHolder = new PeriodHolder(v1);
                break;
            case EMPTYITEM:
                View v2 = inflater.inflate(R.layout.info_about_list_item,viewGroup, false);
                viewHolder = new PeriodHolder(v2);
                break;
            default:
                View v3 = inflater.inflate(R.layout.info_last_item, viewGroup, false);
                viewHolder = new LastItemHolder(v3);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TIMEITEM:
                PeriodHolder vh1 = (PeriodHolder) holder;
                configureViewHolderPeriodItem(vh1, position);
                break;
            case LASTITEM:
                LastItemHolder vh3 = (LastItemHolder) holder;
                configureViewHolderLastItem(vh3);
                break;
            default:
                PeriodHolder vh2 = (PeriodHolder) holder;
                configureViewHolderEmptyItem(vh2);
                break;
        }
    }

    public int getItemViewType (int position){
        if(position==list.size()-1) return LASTITEM;
        else if(position==list.size()-2) return EMPTYITEM;
        else return TIMEITEM;
    }

    @Override
    public int getItemCount() { return list.size(); }

    private void configureViewHolderLastItem(LastItemHolder holder){
        holder.firstrow.setText("В расписании возможны изменения.");
        holder.secondrow.setText("Перед поездкой уточните время отправления.");
        holder.thirdrow.setText("Справочная автовокзала:");
        holder.numberrow.setText("(8362) 45-03-05");
    }

    private void configureViewHolderEmptyItem(PeriodHolder holder){
        holder.hour.setText("");
        holder.minute1.setText("");
        holder.minute2.setText("");
        holder.minute3.setText("");
        holder.minute4.setText("");
        holder.note.setText("");
    }

    private void configureViewHolderPeriodItem ( PeriodHolder holder, int position) {
        Route_Info_More route_info = list.get(position);
        holder.hour.setText(route_info.getHour() + ":");
        int length = route_info.getMinutes().length;
        if (length == 1) {
            holder.minute1.setText(route_info.getMinutes()[0]);
            holder.minute2.setVisibility(View.GONE);
            holder.minute3.setVisibility(View.GONE);
            holder.minute4.setVisibility(View.GONE);
        } else {
            if (length == 2) {
                holder.minute1.setText(route_info.getMinutes()[0]);
                holder.minute2.setText(route_info.getMinutes()[1]);
                holder.minute3.setVisibility(View.GONE);
                holder.minute4.setVisibility(View.GONE);
            } else {
                if (length == 3) {
                    holder.minute1.setText(route_info.getMinutes()[0]);
                    holder.minute2.setText(route_info.getMinutes()[1]);
                    holder.minute3.setText(route_info.getMinutes()[2]);
                    holder.minute4.setVisibility(View.GONE);
                } else {
                    holder.minute1.setText(route_info.getMinutes()[0]);
                    holder.minute2.setText(route_info.getMinutes()[1]);
                    holder.minute3.setText(route_info.getMinutes()[2]);
                    holder.minute4.setText(route_info.getMinutes()[3]);
                }
            }
        }
        if (route_info.getNote() == null) {
            holder.note.setVisibility(View.GONE);
        } else {
            String temparray = route_info.getNote().substring(0, 2);
            String realnote = route_info.getNote().substring(3);
            if (temparray.equals("CO")) holder.note.setText(realnote);
            else {
                if (temparray.equals(route_info.getMinutes()[0])) {
                    holder.note.setText(realnote);
                    holder.note.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                    holder.minute1.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                } else {
                    if (temparray.equals(route_info.getMinutes()[1])) {
                        holder.note.setText(realnote);
                        holder.note.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        holder.minute2.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                    } else {
                        if (temparray.equals(route_info.getMinutes()[2])) {
                            holder.note.setText(realnote);
                            holder.note.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                            holder.minute3.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        } else {
                            holder.note.setText(realnote);
                            holder.note.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                            holder.minute4.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        }
                    }
                }
            }
        }
    }

}
