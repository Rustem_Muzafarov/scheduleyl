package com.dev.klvra.scheduleyl;


public class Route_Info_More_Periodic {
    private String hour;
    private String[] minutes;
    private String note;
    private String period;

    public void  setHour (String hour) {
        this.hour = hour;
    }

    public void  setMinutes (String[] minutes) {
        this.minutes = minutes;
    }

    public void  setNote (String note) {
        this.note = note;
    }

    public void setPeriod (String period){
        this.period = period;
    }

    public String getHour(){
        return hour;
    }

    public String[] getMinutes(){
        return minutes;
    }

    public String getNote(){
        return note;
    }

    public String getPeriod(){
        return period;
    }
}
