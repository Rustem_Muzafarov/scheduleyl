package com.dev.klvra.scheduleyl.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dev.klvra.scheduleyl.fragments.Fragment_Backward;
import com.dev.klvra.scheduleyl.fragments.Fragment_Backward_Periodic;
import com.dev.klvra.scheduleyl.fragments.Fragment_Forward;
import com.dev.klvra.scheduleyl.fragments.Fragment_Forward_Periodic;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    int period;
    String route;

    public ViewPagerAdapter(FragmentManager fm, int period, String route) {
        super(fm);
        this.period = period;
        this.route = route;
    }

    @Override
    public Fragment getItem(int position) {
        if (position==1){       //first tab forward
            if(period==0) {     //item without period name
                Bundle bundle = new Bundle();
                bundle.putString("route", route);
                Fragment_Backward fragment_backward = new Fragment_Backward();
                fragment_backward.setArguments(bundle);
                return fragment_backward;
            }
            else {
                Bundle bundle = new Bundle(); //item with period name
                bundle.putString("route", route);
                Fragment_Backward_Periodic fragment_backward_periodic = new Fragment_Backward_Periodic();
                fragment_backward_periodic.setArguments(bundle);
                return fragment_backward_periodic;
            }
        }
        else{           //second tab backward
            if(period==0) {
                Bundle bundle = new Bundle();
                bundle.putString("route", route);
                Fragment_Forward fragment_forward = new Fragment_Forward();
                fragment_forward.setArguments(bundle);
                return fragment_forward;
            }
            else {
                Bundle bundle = new Bundle();
                bundle.putString("route", route);
                Fragment_Forward_Periodic fragment_forward_periodic = new Fragment_Forward_Periodic();
                fragment_forward_periodic.setArguments(bundle);
                return fragment_forward_periodic;
            }

        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
