package com.dev.klvra.scheduleyl.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.klvra.scheduleyl.adapters.PeriodicRecAdapter;
import com.dev.klvra.scheduleyl.R;
import com.dev.klvra.scheduleyl.RecViewDecoration;
import com.dev.klvra.scheduleyl.Route_Info_More_Periodic;
import com.dev.klvra.scheduleyl.database.DatabaseAccess;

import java.util.ArrayList;

public class Fragment_Backward_Periodic extends Fragment {
    ArrayList<Route_Info_More_Periodic> list;
    RecyclerView recyclerView;
    public int firstNum, secondNum;
    String firstperiod;
    String secondperiod;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MyTag","Backward onCreate()");
        Bundle bundle = getArguments();
        String route = bundle.getString("route");
        final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
        databaseAccess.open();
        list = databaseAccess.getInfoAboutRoutePeriodic(route,"backward");
        databaseAccess.close();
        firstNum=0;
        secondNum=0;
        firstperiod = list.get(0).getPeriod();
        for(int i=0; i<list.size(); i++){
            String temp = list.get(i).getPeriod();
            if(temp.equals(firstperiod)) firstNum++;
            else secondNum++;
        }
        Route_Info_More_Periodic period = new Route_Info_More_Periodic();
        period.setPeriod(firstperiod);
        list.add(0,period);
        if(secondNum!=0) {
            Route_Info_More_Periodic period_2 = new Route_Info_More_Periodic();
            period_2.setPeriod(list.get(firstNum+2).getPeriod());
            list.add(firstNum+1,period_2);
        }
        list.add(list.size(), new Route_Info_More_Periodic());
        list.add(list.size(), new Route_Info_More_Periodic());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_backward, null);
        recyclerView =(RecyclerView) v.findViewById(R.id.recycler_view_backward);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new RecViewDecoration(getActivity()));
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        return v;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PeriodicRecAdapter adapter = new PeriodicRecAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d("MyTag","Backward onResume()");
    }

    public void scrollToTheTop(){
        recyclerView.scrollToPosition(0);
        Log.d("MyTag","scroll Backward");
    }
}
