package com.dev.klvra.scheduleyl;

import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

public class RecViewLastItemOffset extends RecyclerView.ItemDecoration {

    private int lastItemOffset;

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state){
        if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 2) return;

        int childCount = parent.getAdapter().getItemCount();
        int firstVisibleItem = ((LinearLayoutManager) parent.getLayoutManager())
                .findFirstCompletelyVisibleItemPosition();
        int lastVisibleItem = ((LinearLayoutManager) parent.getLayoutManager())
                .findLastCompletelyVisibleItemPosition();

        if((firstVisibleItem==0)&&(lastVisibleItem==(childCount-1))){
            Log.d("MyTag","Fits correct");
            View lastView = parent.getChildAt(childCount-1);
            int listSize = parent.getHeight();
            int lastItemHeight = lastView.getMeasuredHeight();
            lastItemOffset = listSize - lastItemHeight;
            outRect.bottom = lastItemOffset;
            }
        }
    }
