package com.dev.klvra.scheduleyl.adapters;


import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dev.klvra.scheduleyl.R;
import com.dev.klvra.scheduleyl.Route_Common;
import com.dev.klvra.scheduleyl.SchedActivity;

import java.util.ArrayList;

public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.CustomViewHolder> {
    private ArrayList<Route_Common> list = new ArrayList<>();
    private Context context;


    public CustomRecyclerViewAdapter(Context context, ArrayList<Route_Common> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.my_list_item_2, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        Route_Common item = list.get(i);
        customViewHolder.textView.setText(item.getName());
        customViewHolder.checkBox.setChecked(item.getFavorite());
        customViewHolder.textView.setOnClickListener(clickListener);
        customViewHolder.textView2.setOnClickListener(clickListener);
        customViewHolder.checkBox.setOnCheckedChangeListener(checkedChangeListener);
        customViewHolder.textView.setTag(customViewHolder);
        customViewHolder.textView2.setTag(customViewHolder);
        customViewHolder.checkBox.setTag(customViewHolder);
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView textView;
        protected CheckBox checkBox;
        protected TextView textView2;

        public CustomViewHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(R.id.textView1);
            this.checkBox = (CheckBox) view.findViewById(R.id.favorite_button);
            this.textView2 = (TextView) view.findViewById(R.id.textView2);
        }
    }

    View.OnClickListener clickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            CustomViewHolder holder = (CustomViewHolder) v.getTag();
            int position = holder.getAdapterPosition();
            String item = list.get(position).getName();
            int period = list.get(position).getPeriod();
            ((SchedActivity) context).routeClick(item, period);
            ((SchedActivity) context).expandToolbar();
        }
    };

    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener(){

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            CustomViewHolder holder = (CustomViewHolder) buttonView.getTag();
            int position = holder.getAdapterPosition();
            String item = list.get(position).getName();
            ((SchedActivity) context).favoriteStateChange(item, isChecked);
        }
    };

    public void refresh (ArrayList<Route_Common> newlist){
        if(newlist!=null) {
            list.clear();
            list.addAll(newlist);
            notifyDataSetChanged();
        }
    }
}