package com.dev.klvra.scheduleyl.fragments;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.klvra.scheduleyl.R;
import com.dev.klvra.scheduleyl.adapters.ViewPagerAdapter;


public class Fragment_about_Route extends Fragment {

    private TabLayout tabLayout;
    public String route;
    public Bundle bundle;
    public int period;
    public View rootView;
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bundle = getArguments();
        route = bundle.getString("route");
        period = bundle.getInt("period");
        rootView = inflater.inflate(R.layout.fragment_about_route, null, false);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), period, route);
        viewPager.setAdapter(viewPagerAdapter);
        final TabLayout.Tab forward = tabLayout.newTab();
        final TabLayout.Tab backward = tabLayout.newTab();
        backward.setText("Обратно");
        forward.setText("Туда");
        tabLayout.addTab(forward);
        tabLayout.addTab(backward);
        tabLayout.setTabTextColors(Color.WHITE, Color.WHITE);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(),false);
                Fragment page = getChildFragmentManager()
                        .findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.getCurrentItem());
                if (page instanceof Fragment_Backward_Periodic){
                    Fragment_Backward_Periodic fragment = (Fragment_Backward_Periodic) page;
                    fragment.scrollToTheTop();
                }
                if (page instanceof Fragment_Forward_Periodic){
                    Fragment_Forward_Periodic fragment = (Fragment_Forward_Periodic) page;
                    fragment.scrollToTheTop();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Fragment page = getChildFragmentManager()
                        .findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.getCurrentItem());
                if (page instanceof Fragment_Backward_Periodic){
                    Fragment_Backward_Periodic fragment = (Fragment_Backward_Periodic) page;
                    fragment.scrollToTheTop();
                }
                if (page instanceof Fragment_Forward_Periodic){
                    Fragment_Forward_Periodic fragment = (Fragment_Forward_Periodic) page;
                    fragment.scrollToTheTop();
                }
            }
        });
        return rootView;
    }
}
