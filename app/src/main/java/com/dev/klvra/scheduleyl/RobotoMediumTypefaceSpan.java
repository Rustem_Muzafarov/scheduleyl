package com.dev.klvra.scheduleyl;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

/**
 * Created by KlvrA on 23.06.2016.
 */
public class RobotoMediumTypefaceSpan extends TypefaceSpan {
    private final Typeface typeface;

    public RobotoMediumTypefaceSpan(String family, Typeface type){
        super(family);
        typeface = type;
    }

    @Override
    public void updateDrawState(TextPaint ds){
        applyCustomTypeface(ds, typeface);
    }

    @Override
    public void updateMeasureState(TextPaint paint){
        applyCustomTypeface(paint, typeface);
    }

    private static void applyCustomTypeface (Paint paint, Typeface tf){
        int oldStyle;
        Typeface old = paint.getTypeface();
        if(old==null) oldStyle = 0;
        else oldStyle = old.getStyle();
        int fake = oldStyle & ~tf.getStyle();
        if((fake & Typeface.BOLD) !=0) {
            paint.setFakeBoldText(true);
        }

        if((fake & Typeface.ITALIC)!=0){
            paint.setTextSkewX(-025.f);
        }

        paint.setTypeface(tf);
    }
}
