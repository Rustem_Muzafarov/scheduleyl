package com.dev.klvra.scheduleyl.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.klvra.scheduleyl.adapters.NonPeriodicRecAdapter;
import com.dev.klvra.scheduleyl.R;
import com.dev.klvra.scheduleyl.RecViewDecoration;
import com.dev.klvra.scheduleyl.RecViewLastItemOffset;
import com.dev.klvra.scheduleyl.Route_Info_More;
import com.dev.klvra.scheduleyl.database.DatabaseAccess;

import java.util.ArrayList;

public class Fragment_Backward extends Fragment {
    ArrayList<Route_Info_More> list;
    RecyclerView recyclerView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        String route = bundle.getString("route");
        final DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
        databaseAccess.open();
        list = databaseAccess.getInfoAboutRoute(route,"backward");
        databaseAccess.close();
        list.add(list.size(), new Route_Info_More());
        list.add(list.size(), new Route_Info_More());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_backward, null);
        recyclerView =(RecyclerView) v.findViewById(R.id.recycler_view_backward);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new RecViewDecoration(getActivity()));
        recyclerView.addItemDecoration(new RecViewLastItemOffset());
        return v;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        NonPeriodicRecAdapter adapter = new NonPeriodicRecAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume(){
        super.onResume();
        recyclerView.getLayoutManager().scrollToPosition(0);
    }
}
