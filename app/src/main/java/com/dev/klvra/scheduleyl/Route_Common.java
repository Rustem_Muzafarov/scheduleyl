package com.dev.klvra.scheduleyl;


public class Route_Common {

    private String name;
    private boolean favorite;
    private int period;

    public Route_Common( String name, boolean favorite, int period){
        this.name = name;
        this.favorite = favorite;
        this.period = period;
    }

    public void  setName (String name) {
        this.name = name;
    }

    public void  setFavorite (boolean favorite) {
        this.favorite = favorite;
    }

    public void  setPeriod (int period) {
        this.period = period;
    }

    public String getName(){
        return name;
    }
    public boolean getFavorite(){
        return favorite;
    }
    public int getPeriod(){
        return period;
    }
}
