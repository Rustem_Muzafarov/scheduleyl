package com.dev.klvra.scheduleyl;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class RecViewDecoration extends RecyclerView.ItemDecoration {

    private static final int[] Attributes = new int[] {android.R.attr.listDivider};
    private Drawable divider;
    private boolean isPeriodic;
    private int firstperiod;

    public RecViewDecoration (Context context){
        final TypedArray styledAttributes = context.obtainStyledAttributes(Attributes);
        divider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    /*public RecViewDecoration (Context context, boolean isPeriodic, int firstperiod ){
        final TypedArray styledAttributes = context.obtainStyledAttributes(Attributes);
        divider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
        this.isPeriodic = isPeriodic;
        this.firstperiod = firstperiod;
    }*/

    @Override
    public void onDraw (Canvas c, RecyclerView parent, RecyclerView.State state) {

            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
        if(isPeriodic) {
            for (int i = 0; i < childCount - 1; i++) {
                if(i!=firstperiod+1) {
                    View child = parent.getChildAt(i);


                    RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                    int top = child.getBottom() + params.bottomMargin;
                    int bottom = top + divider.getIntrinsicHeight();

                    divider.setBounds(left, top, right, bottom);
                    divider.draw(c);
                }
            }
        }
        else {
            for (int i = 0; i < childCount - 1; i++) {
                View child = parent.getChildAt(i);
                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + divider.getIntrinsicHeight();
                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }
}
