package com.dev.klvra.scheduleyl.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dev.klvra.scheduleyl.Route_Common;
import com.dev.klvra.scheduleyl.Route_Info_More;
import com.dev.klvra.scheduleyl.Route_Info_More_Periodic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;
    Route_Info_More route_info_more;
    Route_Info_More_Periodic route_info_more_periodic ;


    private DatabaseAccess(Context context) {this.openHelper = new DatabaseOpenHelper(context);}

    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    public void open() {this.database = openHelper.getReadableDatabase();}

    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public ArrayList<Route_Common> getRoutesList(String type) {
        ArrayList<Route_Common> list = new ArrayList<>();
        String type_array[] = {type};
        String columns[] = {"name","favorite","period"};
        Cursor c = database.query("common_info", columns, "type = ?", type_array, null, null, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String name = c.getString(c.getColumnIndex("name"));
            int temp = c.getInt(c.getColumnIndex("favorite"));
            int period = c.getInt(c.getColumnIndex("period"));
            boolean favorite;
            if(temp==1) favorite = true;
            else favorite = false;
            Route_Common route_common = new Route_Common(name, favorite, period);
            list.add(route_common);
            c.moveToNext();
        }
        c.close();
        return listSort(list);
    }

    public ArrayList<Route_Info_More> getInfoAboutRoute(String name, String direction){
        ArrayList<Route_Info_More> list_more = new ArrayList<>();
        String type_array[] = {direction};
        String columns[] = {"hour","minute","note"};
        Cursor c = database.query("\""+ name +"\"",
                columns,
                "direction = ?",
                type_array,
                null, null, null);
        if(c!=null){
            if (c.moveToFirst()) {
                do{
                route_info_more = new Route_Info_More();
                route_info_more.setHour(c.getString(c.getColumnIndex("hour")));
                route_info_more.setMinutes(c.getString(c.getColumnIndex("minute")).split(","));
                for(int j=0;j<route_info_more.getMinutes().length;j++) route_info_more.getMinutes()[j] = route_info_more.getMinutes()[j].trim();
                route_info_more.setNote(c.getString(c.getColumnIndex("note")));
                list_more.add(route_info_more);
                }
                while(c.moveToNext());
            }
        }
        c.close();
        return list_more;
    }

    public ArrayList<Route_Common> searchInDB(String route){
        ArrayList<Route_Common> list = new ArrayList<>();
        String columns[] = {"name","favorite","period"};
        Cursor c = database.query("common_info",
                columns,
                "name" + " LIKE ?",
                new String[]{"%" + route + "%"},
                null, null, null);
        if (c.moveToFirst()) {
            do {
                String name = c.getString(c.getColumnIndex("name"));
                int temp = c.getInt(c.getColumnIndex("favorite"));
                int period = c.getInt(c.getColumnIndex("period"));
                boolean favorite;
                if (temp == 1) favorite = true;
                else favorite = false;
                Route_Common route_common = new Route_Common(name, favorite, period);
                list.add(route_common);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public void changeFavoriteStatusToTrue(String route){ //changing favorite status to true
        ContentValues cv = new ContentValues();
        cv.put("favorite","1");
        database.update("common_info", cv, "name = ?", new String[]{route});
    }

    public void changeFavoriteStatusToFalse(String route){      //... to false
        ContentValues cv = new ContentValues();
        cv.put("favorite","0");
        database.update("common_info", cv, "name = ?", new String[]{route});
    }

    public ArrayList<Route_Common> getFavoriteList() {
        ArrayList<Route_Common> list = new ArrayList<>();
        String type_array[] = {"1"};
        String columns[] = {"name","favorite","period"};
        Cursor c = database.query("common_info", columns, "favorite = ?", type_array, null, null, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String name = c.getString(c.getColumnIndex("name"));
            int temp = c.getInt(c.getColumnIndex("favorite"));
            int period = c.getInt(c.getColumnIndex("period"));
            boolean favorite;
            if(temp==1) favorite = true;
            else favorite = false;
            Route_Common route_common = new Route_Common(name, favorite, period);
            list.add(route_common);
            c.moveToNext();
        }
        c.close();

        Collections.sort(list, new Comparator<Route_Common>() {
            @Override
            public int compare(Route_Common lhs, Route_Common rhs) {
                String lhsname = lhs.getName();
                String rhsname = rhs.getName();
                return lhsname.compareTo(rhsname);
            }
        });
        return list;
    }

    public ArrayList<Route_Info_More_Periodic> getInfoAboutRoutePeriodic (String name, String direction){
        ArrayList<Route_Info_More_Periodic> list_more = new ArrayList<>();
        String type_array[] = {direction};
        String columns[] = {"hour","minute","note","period"};
        Cursor c = database.query("\""+ name +"\"",
                columns,
                "direction = ?",
                type_array,
                null, null, null);
        if(c!=null){
            if (c.moveToFirst()) {
                do{
                    route_info_more_periodic = new Route_Info_More_Periodic();
                    route_info_more_periodic.setHour(c.getString(c.getColumnIndex("hour")));
                    route_info_more_periodic.setMinutes(c.getString(c.getColumnIndex("minute")).split(","));
                    for(int j=0;j<route_info_more_periodic.getMinutes().length;j++) route_info_more_periodic.getMinutes()[j] = route_info_more_periodic.getMinutes()[j].trim();
                    route_info_more_periodic.setNote(c.getString(c.getColumnIndex("note")));
                    route_info_more_periodic.setPeriod(c.getString(c.getColumnIndex("period")));
                    list_more.add(route_info_more_periodic);
                }
                while(c.moveToNext());
            }
        }
        c.close();
        return list_more;
    }

    public ArrayList<Route_Common> listSort (ArrayList<Route_Common> list) {
        ArrayList<Route_Common> favorlist = new ArrayList<>();
        for(int i=list.size()-1; i!=-1; i--){
            Route_Common route = list.get(i);
            if(route.getFavorite()){
                favorlist.add(list.get(i));
                list.remove(i);
            }
        }
        for(int i=0; i<favorlist.size();i++) list.add(0,favorlist.get(i));

        return list;
    }
}
